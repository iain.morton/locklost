import numpy as np
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec

from gwpy.segments import Segment
from gwpy.timeseries import TimeSeries

from .. import logger
from .. import config
from .. import data
from .. import plotutils


ASD_CHANNEL = [
    '{}:OMC-PI_DCPD_64KHZ_AHF_DQ'.format(config.IFO)
]
ASD_WINDOW = [-(8*60), 0]


##############################################


def check_pi(event):
    """Checks BAND 1-7 log channels for pi.

    Checks pi band  goes above threshold and
    creates a tag/table if above a certain threshold.

    """
    plotutils.set_rcparams()

    mod_window = [config.PI_SEARCH_WINDOW[0], config.PI_SEARCH_WINDOW[1]]
    segment = Segment(mod_window).shift(int(event.gps))
    PI_channels = data.fetch(config.PI_CHANNELS, segment)

    mod_window_asd = [ASD_WINDOW[0], ASD_WINDOW[1]]
    segment_asd = Segment(mod_window_asd).shift(int(event.gps))
    ASD_data = data.fetch(ASD_CHANNEL, segment_asd)[0]
    asd_timeseries = TimeSeries(
        ASD_data.data,
        t0=segment_asd[0],
        dt=1/ASD_data.sample_rate
    )
    time_frame = 33 * 2
    fft_length = 16
    overlap = fft_length / 2
    before_pi = asd_timeseries.times.value - event.gps < ASD_WINDOW[0] + time_frame
    during_pi = asd_timeseries.times.value - event.gps > -time_frame
    asd_before = asd_timeseries[before_pi].asd(fftlength=fft_length, overlap=overlap)
    asd_during = asd_timeseries[during_pi].asd(fftlength=fft_length, overlap=overlap)

    saturating = False
    for buf in PI_channels:
        srate = buf.sample_rate
        t = np.arange(segment[0], segment[1], 1/srate)
        idxs = ((t-event.gps) < -2) & ((t-event.gps) > -20)
        if np.max(buf.data[idxs]) > config.PI_SAT_THRESH:
            saturating = True

    if saturating:
        # ADD H1 code here
        if config.IFO == 'L1':
            if event.transition_index[0] >= 1300:
                logger.info('PI Tag not set up')
                # event.add_tag('PI_MONITOR')
    else:
        logger.info('no PI')

    gs = gridspec.GridSpec(2, 4)
    gs.update(wspace=0.5)
    fig = plt.figure(figsize=(22*3, 16*3))
    ax1 = fig.add_subplot(gs[0, :2])
    ax2 = fig.add_subplot(gs[0, 2:])
    ax3 = fig.add_subplot(gs[1, :])
    for idx, buf in enumerate(PI_channels):
        srate = buf.sample_rate
        t = np.arange(segment[0], segment[1], 1/srate)
        ax1.plot(
            t-event.gps,
            buf.data,
            label=buf.channel,
            alpha=0.8,
            lw=2,
        )
        ax2.plot(
            t-event.gps,
            buf.data,
            label=config.PI_DICT[buf.channel],
            alpha=0.8,
            lw=2,
        )
    ax1.axhline(
        config.PI_SAT_THRESH,
        linestyle='--',
        color='black',
        label='PI threshold',
        lw=5,
    )
    ax2.axhline(
        config.PI_SAT_THRESH,
        linestyle='--',
        color='black',
        label='PI threshold',
        lw=5,
    )
    ax1.set_xlabel('Time [s] since lock loss at {}'.format(event.gps), labelpad=10)
    ax2.set_xlabel('Time [s] since lock loss at {}'.format(event.gps), labelpad=10)
    ax1.set_ylabel('Band-Limited RMS [log]')
    ax2.set_ylabel('Band-Limited RMS [log]')
    ax1.set_xlim(t[0]-event.gps, t[-1]-event.gps)
    ax2.set_xlim(-20, 5)
    ax1.set_ylim(0, 1)
    ax2.set_ylim(0, 1)
    ax1.legend(loc='best')
    ax2.legend(loc='best')
    ax1.set_title('PI BLRMS Monitors')
    ax2.set_title('PI BLRMS Monitors')
    ax1.grid()
    ax2.grid()

    ax3.plot(
        asd_during.frequencies / 1000,
        asd_during,
        linewidth=1,
        color='red',
        label='Right before lockloss'
    )
    ax3.plot(
        asd_before.frequencies / 1000, asd_before,
        linewidth=1,
        color='lightsteelblue',
        label='8 min before lockloss'
    )
    ax3.set_yscale('log')
    ax3.set_xscale('log')
    ax3.set_xlim(5, 32)
    ax3.set_title('ASD of {}'.format(ASD_data.channel))
    ax3.set_ylabel('Magnitude')
    ax3.set_xlabel('Frequency [kHz]')
    ax3.legend(loc='best')
    ax3.set_xticks(
        np.arange(5, 32),
        labels=['' if x % 5 != 0 else x for x in np.arange(5, 32)]
    )
    ax3.grid()

    outfile_plot = 'PI_monitor.png'
    outpath_plot = event.path(outfile_plot)
    fig.savefig(outpath_plot, bbox_inches='tight')
