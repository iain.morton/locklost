import numpy as np
import matplotlib.pyplot as plt

from gwpy.segments import Segment

from .. import logger
from .. import config
from .. import data
from .. import plotutils


FSS_SEARCH_WINDOW = [-60, 5]
FSS_CHANNELS = [
    '{}:PSL-ISS_AOM_DRIVER_MON_OUT_DQ'.format(config.IFO)
]
FSS_THRESH = 0.36


##############################################


def check_iss(event):
    """Checks for ISS AOM.

    Checks ISS AOM for counts below threshold and
    creates a tag/plot if below said threshold.

    """
    plotutils.set_rcparams()

    mod_window = [FSS_SEARCH_WINDOW[0], FSS_SEARCH_WINDOW[1]]
    segment = Segment(mod_window).shift(int(event.gps))
    buf = data.fetch(FSS_CHANNELS, segment)[0]

    srate = buf.sample_rate
    t = np.arange(segment[0], segment[1], 1/srate)
    idxs = (t-event.gps) < -1
    if config.IFO == 'L1':
        if np.min(buf.data[idxs]) <= FSS_THRESH:
            event.add_tag('ISS')
        else:
            logger.info('no ISS issue')
    else:
        logger.info('Tag not Configured for LHO')
    fig, ax = plt.subplots(1, figsize=(22, 16))
    t = np.arange(segment[0], segment[1], 1/srate)
    ax.plot(
        t-event.gps,
        buf.data,
        label=buf.channel,
        alpha=0.8,
        lw=2,
    )
    if config.IFO == 'L1':
        ax.axhline(
            FSS_THRESH,
            linestyle='--',
            color='black',
            label='ISS Threshold',
            lw=5,
        )
    ax.grid()
    ax.set_xlabel('Time [s] since lock loss at {}'.format(event.gps), labelpad=10)
    ax.set_ylabel('Voltage [V]')
    ax.legend(loc='best')
    ax.set_title('ISS AOM', y=1.04)
    ax.set_xlim(segment[0]-event.gps, segment[1]-event.gps)

    fig.tight_layout()

    fig.savefig(event.path('iss.png'))
