import os
import signal
import logging

from . import config

import matplotlib
matplotlib.use('Agg')

try:
    from .version import version as __version__
except ImportError:
    __version__ = '?.?.?'


logger = logging.getLogger('LOCKLOST')


# signal handler kill/stop signals
def signal_handler(signum, frame):
    try:
        signame = signal.Signal(signum).name
    except AttributeError:
        signame = signum
    logger.error("Signal received: {} ({})".format(signame, signum))
    raise SystemExit(1)


def set_signal_handlers():
    signal.signal(signal.SIGINT, signal_handler)
    signal.signal(signal.SIGTERM, signal_handler)
    signal.signal(eval('signal.'+config.CONDOR_KILL_SIGNAL), signal_handler)


def config_logger(level=None):
    if not level:
        level = os.getenv('LOG_LEVEL', 'INFO')
    handler = logging.StreamHandler()
    if os.getenv('LOG_FMT_NOTIME'):
        fmt = config.LOG_FMT_NOTIME
    else:
        fmt = config.LOG_FMT
    formatter = logging.Formatter(fmt)
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    logger.setLevel(level)
